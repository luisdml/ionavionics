// this is the code for the avionics ebay for ion project v2
// version 1.0.13
// launch detection and appoge detection only working with altitude
// bluetooth coms working


#include <Arduino.h>
#include <stdio.h>
#include <Wire.h>
#include <SoftwareSerial.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP085.h>
#include <Adafruit_ADXL345_U.h>
#include <SD.h>
//#include <time.h>

// *** ******************************************* ***
// hardware needed defines and settings **************

// I2C communication: A4 ->SDA
// I2C communication: A5 ->SCL

Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345); /* Assign a unique ID to this sensor at the same time */
Adafruit_BMP085 bmp;                                              /* Barometer variable */

// if serial defined all coms done through serial else through bluetooth
#define ION_SERIAL // if set make sure bluetooth is not connected

/*Bluetooth settings*/
#define bluetoothTx 5       // TX-O pin of bluetooth mate, Arduino D2
#define bluetoothRx 6       // RX-I pin of bluetooth mate, Arduino D3

SoftwareSerial bluetooth(bluetoothTx, bluetoothRx);

#define buzzer 10     // Buzzer pin
#define relay1 8      // Relay pin 8
#define relay2 9      // Relay pin 9

#define armedButton 2 // Arm / disarm button
#define armedLed 3    // armed led
/*
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4 (for MKRZero SD: SDCARD_SS_PIN)
 */
#define sdCard 4

// *** ******************************************* ***
// logical needed defines and settings **************

#define APPOGE_THRESHOLD        5
#define LAUNCH_ALTITUDE         5
#define debounceTimeThreshold  250

// changed the defines below to binary since the error varialble is unsigned short
#define NO_ERROR        0b000000      // errors should increment by mag orders of 10
#define ACCEL_ERROR     0b000001      // no ADXL345 detected or ADXL345 related error
#define ALTIM_ERROR     0b000010      // altimeter error
#define SERIALCOM_ERROR 0b000100      // Serial com Error
#define BLUETOOTH_ERROR 0b001000      // Bluetooth com Error
#define PARACHUTE_ERROR 0b010000      // Parachute deployment error
#define SD_ERROR        0b100000      // SD error

// function definitions
bool DeployPrimaryParachute (int altitude, short int InFlight, short int ReachedAppoge, short int ParachuteDeployed);
bool DetectAppoge(int altitude);
bool DetectLauch (int altitude); // there is another detect launch with accelerometer
void parachuteDeployment(void);
void arm(void);
int SetGroundZero(void);

// loging fucntions
void LogData(sensors_event_t acc, int altitude, bool InFlight, bool ParachuteDeployed,bool ReachedAppoge,char *msg);
void serialLogData(char *str);
//void GetUniqueFileName (void);


// tone functions and settings // none of these are working
void buzzerWarning(int errorNumber);
void armTone(void);
void disarmTone(void);
void findMeTone(void);
bool playArmTone=false;
bool playDisarmTone=false;


// config functions
void InitAltim(void);
void InitAccelerometer(void);
void InitComs(void);
void ConfigBlueTooth(void);
void InitSerial(void);
void ConfigInputOutput(void);
void InitSD(void);

//Intterruption settings
volatile bool Armed = false;
long debounceTimeCounter = 0;

// global variables

unsigned short Error = NO_ERROR;  // set if an error has been found.   init to 0 becasue all systems will be checked in setup

File logFile;
char LogFileName[]="IonLogFile.txt";          // Name of the log file writen on the sd card

int MaxAltitude = 0;
int groundZero = 0;

bool parachuteDeployed = false;
long startTime = 0;         // time when armed
long startMissionTime = 0;  // time when launch detected
long appogeTime = 0;        // time when appoge was detected

void setup()
{
  // init system and sensors
  InitComs();           // first init comms if no comms nothing else works  
  //InitSD();             // init sd module
  InitAccelerometer();  // init acelerometer
  InitAltim();          // init altimeter
  
  // When the armed button is clicked, then the arm function is called to arm or disarm the system
  attachInterrupt(digitalPinToInterrupt(armedButton), arm, FALLING);

  groundZero = SetGroundZero();
  // done with setup.... proceed to main loop
  serialLogData("Setup Completed...  Systems GO");
}

void loop() { // While energized this loop will always run
  
  static bool InFlight = false;           // true if launch detected.
  static bool ParachuteDeployed = false;  // true if parachute has been deployed
  static bool ReachedAppoge = false;      // true if rocket has ReachedAppoge
  int altitude = 0;                       // current altitude
  sensors_event_t event;                  // sensor acceleration variables

  accel.getEvent(&event);  // get accelerometer event: 
  altitude = (int) bmp.readAltitude(101500) - groundZero; // Get relative altitude
  if (Armed) {        // every thing happens only when armed.
   /* if(playArmTone) {
      armTone();
      playArmTone=false;
    }*/
    if (altitude > MaxAltitude) {
      MaxAltitude = altitude;
    }
    if (!InFlight) { // if not in flight, only check for launch
      LogData(event, altitude,InFlight, ParachuteDeployed,ReachedAppoge,"Armed and Ready for launch");
      InFlight = DetectLauch (altitude);
    }             // verify if launched
    else {
      LogData(event, altitude,InFlight, ParachuteDeployed,ReachedAppoge,"In flight");
      if (!ParachuteDeployed) {
        ReachedAppoge = DetectAppoge (altitude);
      }   // check for appoge
      if ((ReachedAppoge) && (!ParachuteDeployed))  {
        if (!ParachuteDeployed) {
          ParachuteDeployed = DeployPrimaryParachute (altitude, InFlight, ReachedAppoge, ParachuteDeployed);
        } // if Parachute not deployed try to deploy
        LogData(event, altitude,InFlight, ParachuteDeployed,ReachedAppoge,"Appoge");
      }        // deploy parachute
    }
  }
  else {
/*    if(playDisarmTone) {
      disarmTone();
      playDisarmTone=false;
    }*/
   //  LogStatus (altitude, InFlight, ReachedAppoge, ParachuteDeployed, "System NOT Armed");
  } 
}

bool DetectAppoge(int altitude)
{
   if (altitude < MaxAltitude - APPOGE_THRESHOLD){
    serialLogData("Appoge detected");
    appogeTime = millis();
    return true;
  }
  return false;
}

bool DetectLauch (int altitude) {        // here we cuould do something else to validate launch
  if (altitude >= LAUNCH_ALTITUDE) {
    serialLogData("Launch Detected");
    startMissionTime = millis();
    return true;
  }
  return false;
}

bool DeployPrimaryParachute (int altitude, short int InFlight, short int ReachedAppoge, short int ParachuteDeployed){
   // here is the code to set the parachute charge off
  sensors_event_t event;
  accel.getEvent(&event);
  LogData(event, altitude,InFlight, ParachuteDeployed,ReachedAppoge,"Starting parachute deployment"); // record acceleration readings before deploying

  digitalWrite(relay1, LOW);
  parachuteDeployed = true;
  long start = millis();
  while(millis()-start < 5000) {  // look at the next 5 secs while deploying
     accel.getEvent(&event);
     LogData(event, altitude,InFlight, ParachuteDeployed,ReachedAppoge,"deploying...");
     delay(100);
  }
  digitalWrite(relay1, HIGH); // here we assume that the parachute has been deployed
  accel.getEvent(&event);
  if (true) {     // if parachute deployment has been detected
    LogData(event, altitude,InFlight, ParachuteDeployed,ReachedAppoge,"Parachute successfully deployed"); 
    return true;
  }
  LogData(event, altitude,InFlight, ParachuteDeployed,ReachedAppoge,"ERROR in deploying parachute"); 
  Error += PARACHUTE_ERROR;
  return false;
}

void arm(void) {
  if (millis() > debounceTimeCounter + debounceTimeThreshold) // check min elapsed time to toggle switch
  {
    Armed = !Armed;
    if (Armed) {
      serialLogData("System Armed");
//      armTone();
    } else {
      serialLogData("System Disarmed");
//      armTone();
    }
    //playArmTone=Armed;
    //playDisarmTone=!Armed;
    debounceTimeCounter = millis();
    digitalWrite(armedLed, Armed); // togle armed led on / off
  }
  startTime = millis(); // start mission time
}

int SetGroundZero(void) { // get an average positive altitue reading
  int i;
  float AvgAlt = 0;
  int TmpAlt=0;
  char msg[64];
  for (i=0, AvgAlt=0;i<6;i++) {
    TmpAlt = (int)bmp.readAltitude(101500);     //Set altitude ground zero
    AvgAlt += TmpAlt;
    delay(100);
  }
  AvgAlt = AvgAlt / i;
  sprintf(msg,"Ground Zero Average Altitude: %d",(int)AvgAlt);
  serialLogData(msg);
  if(AvgAlt < 0)
    return 10000; // return a weird number on ERROR
  return AvgAlt;
}

// loging fuctions
  
void LogData(sensors_event_t event, int altitude, bool InFlight, bool ParachuteDeployed,bool ReachedAppoge,char *msg) {
  static short c = 5;
  int acx = (int) (event.acceleration.x * 10.0); // first multiply by 10 then convert to int
  int acy = (int) (event.acceleration.y * 10.0);
  int acz = (int) (event.acceleration.z * 10.0);

  if (--c>0) return; // dont do anything 4/5 times to be considered later
  else {
    char str[256]; // note that sprintf does not take floats so they have to be converted to integer
    int err = sprintf(str,"acx: %+5d acy: %+5d acz: %+5d alt: %5d InFlight: %1d Parachute: %1d Appoge: %1d, %s",
          acx,acy,acz,altitude,InFlight,ParachuteDeployed, ReachedAppoge,msg);
    serialLogData(str);
    c = 5;
  } 
}

void serialLogData(char *str) {
  #ifdef ION_SERIAL     // if serial coms defined then log to serial
    Serial.println(str);
  #else             // else log to bluetooth
    bluetooth.println(str); // write to bluetooth
  #endif
  // log to file always
/*  dataFile = FileSystem.open("/mnt/sd/GPSdatalog.txt", FILE_APPEND);
  if (logFile) {
    logFile.println(str);
    logFile.close();
  }
  else {
    Error += SD_ERROR;
  }*/
}

/*void GetUniqueFileName (void) {
  time_t t = time (NULL);
  struct tm tm = *localtime (&t);
  // the following command shoudl not exceed 30 chars
  sprintf (LogFileName, "IonLog:%02d.%02d.%04d.%d:%02d.txt", tm.tm_mon + 1,
     tm.tm_mday, tm.tm_year + 1900, tm.tm_hour, tm.tm_min);
  // no need to return anything   
}*/


// tone functions below need work

void buzzerWarning(int errorNumber) {
  for (int i = 0; i < errorNumber; i++) {
    tone(buzzer,440);
    delay(1000);
    noTone(buzzer);
    delay(1000);
  }
}

void armTone(void) {
  tone(buzzer,3000,600);
  delay(600);
  digitalWrite(buzzer, HIGH);
}

void disarmTone(void) {
  tone(buzzer, 1319, 600);
  delay(600);
  digitalWrite(buzzer, HIGH);
}

void findMeTone(void){
   for(int i=0; i<3; i++) {
     tone(buzzer,440,500);
     delay(500);
     noTone(buzzer);
     delay(500);
   }
}

// functions below are config functions... these only run if no com errors were found

void InitAltim(void) {
  sensors_event_t aux;
  if (!bmp.begin()) { // there was a problem initializing BMP085...   check your connections
      Error += ALTIM_ERROR;
  } else {
      LogData(aux, 0, false, false,false,"BMP 085 detected... values still incorrect");
  }
}

void InitAccelerometer(void) { // Initialize ADXL 345 sensor....  this is done only once
  sensors_event_t aux;
  if(!accel.begin()){ // There was a problem initializing the ADXL345 ... check your connections 
    Error += ACCEL_ERROR; // add accelerometer error  
  } else {
      LogData(aux, 0, false, false,false,"ADXL 345 detected... values still incorrect");
  }
}

void InitSD(void) { // Initialie SD module... this is only done once.
  sensors_event_t aux;
  char str[256];
  Serial.println("Entering SD init");
  if (!SD.begin(sdCard)){ //There was a problem initializing the SD module.
    Error += SD_ERROR;
    LogData(aux, 0, false, false,false,"error configuring SD module");          
  } else {
      sprintf(str,"acx:       acy:       acz:       alt:          InFlight:     Parachute:     Appoge:   ");
      // Now open the file and write the first line ever on the top of the file
      //GetUniqueFileName();
      logFile = SD.open(LogFileName, FILE_WRITE);  
      logFile.println(str);
      logFile.close();
      LogData(aux, 0, false, false,false,"SD module configured correctly");          
  }
}

// config coms... only configure Serial or bluetoot... if no serial is defined, bluetooth is configured

void InitComs(void) {
  #if defined(ION_SERIAL) // if serial coms defined then init serial for debuging purposes
    InitSerial();
  #else
      ConfigBlueTooth();    // config bluetooth
  #endif
  ConfigInputOutput();  // set input output pins
}

void ConfigBlueTooth(void) { // config bluetooth coms
  bluetooth.begin(115200);    // The Bluetooth Mate defaults to 115200bps
  bluetooth.print("$");       // Print three times individually
  bluetooth.print("$");
  bluetooth.print("$");       // Enter command mode
  delay(100);                 // Short delay, wait for the Mate to send back CMD
  bluetooth.println("U,9600,N");  // Temporarily Change the baudrate to 9600, no parity
  bluetooth.begin(9600);
  // how can we find an error when configuring bluetooth?
}

void InitSerial(void) { // Init serial com
  #ifndef ESP8266
  while (!Serial); // wait for serial com initialization from for Leonardo/Micro/Zero
  #endif
  Serial.begin(9600);
  Serial.println(F("Innitializing serial coms"));
  while (Error) { // on error just stay here forever...   log, print, beep
    Serial.println(F("ERROR on setup.  Cannot continue: "));
    Serial.println(Error);
    delay(5);
  }  
}

// aruduino hardware config

void ConfigInputOutput(void) { // set input ouput arduino pins
  accel.setRange(ADXL345_RANGE_16_G);
  pinMode(buzzer, OUTPUT);
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);
  digitalWrite(relay1, HIGH);
  digitalWrite(relay2, HIGH);
  pinMode(armedButton, INPUT_PULLUP);
  pinMode(armedLed, OUTPUT);
  digitalWrite(buzzer, HIGH);
}
